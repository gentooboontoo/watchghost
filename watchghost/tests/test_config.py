from unittest import mock

import pytest
import toml
from watchghost.config import read


@pytest.fixture
def default_groups():
    return {'all': ['localhost']}


@pytest.fixture
def default_loggers():
    return {'console': {'type': 'Console'}}


@pytest.fixture
def default_servers():
    return {'localhost': {
        'name': 'localhost', 'ipv4': '127.0.0.1', 'ipv6': '::1'
    }}


@pytest.fixture
def default_watchers():
    return {'local': {
        'description': 'http://localhost:8888/',
        'service': 'network.HTTP',
        'server': 'localhost',
        'url': 'http://localhost:8888/'
    }}


@pytest.fixture
def config_factory(
        tmpdir,
        default_groups, default_loggers, default_servers, default_watchers
):
    def create_config_dir(configs=None):
        config_dir = tmpdir.mkdir("config")
        if configs is None:
            configs = {}
        configs.setdefault('groups', default_groups)
        configs.setdefault('loggers', default_loggers)
        configs.setdefault('servers', default_servers)
        configs.setdefault('watchers', default_watchers)
        for config_name, config_params in configs.items():
            tmp_file = config_dir.join('{}.toml'.format(config_name))
            tmp_file.write(toml.dumps(config_params))
        return str(config_dir), configs

    return create_config_dir


@mock.patch('watchghost.config.Logger.create')
@mock.patch('watchghost.config.Server')
@mock.patch('watchghost.config.Watcher')
@mock.patch('watchghost.config.Service')
@mock.patch(
    'watchghost.config.app',
    services={}, watchers={}, servers={}, loggers=[]
)
def test_read_create_logger_server_service_watcher_default(
        app_mock, service_mock, watcher_mock,
        server_mock, create_logger_mock, config_factory
):
    config_dir, configs = config_factory()
    read(config_dir)
    create_logger_mock.assert_has_calls([
        mock.call(config) for config in configs['loggers'].values()
    ])
    server_mock.assert_has_calls([
        mock.call(name, config) for name, config in configs['servers'].items()
    ])
    service_mock.assert_has_calls([
        mock.call(
            config.get('service'),
            group=config.get('group'),
            server=config.get('server')
        )
        for name, config in configs['watchers'].items()
    ])
    watcher_mock.assert_called_once_with(
        app_mock.servers['localhost'],
        app_mock.services['network.HTTP'],
        configs['watchers']['local'],
        app_mock.loggers
    )


@mock.patch('watchghost.config.Logger.create')
@mock.patch('watchghost.config.Server')
@mock.patch('watchghost.config.Watcher')
@mock.patch('watchghost.config.Service')
@mock.patch(
    'watchghost.config.app',
    services={}, watchers={}, servers={}, loggers=[]
)
def test_read_uses_name_if_no_description_provided_for_watcher(
        app_mock, service_mock, watcher_mock,
        server_mock, create_logger_mock, config_factory
):
    config_dir, configs = config_factory({'watchers': {
        'local': {
            'service': 'network.HTTP',
            'server': 'localhost',
            'url': 'http://localhost:8888/'
        }
    }})
    read(config_dir)
    watcher_mock.assert_called_once_with(
        app_mock.servers['localhost'],
        app_mock.services['network.HTTP'],
        {**configs['watchers']['local'], 'description': 'local'},
        app_mock.loggers
    )
