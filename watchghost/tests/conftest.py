from uuid import uuid4

import pytest
from aiohttp import web
from watchghost import config
from watchghost import web as wg_web


@pytest.fixture
def create_app(loop):
    app = web.Application()
    config.read('/tmp/pytest/{}'.format(uuid4()))
    app.add_routes(wg_web.routes)
    return app
