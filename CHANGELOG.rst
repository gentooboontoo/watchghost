Changelog
=========

Version 0.4.0
-------------

Released on 2020-02-xx

Changes:

- Add tests and Python 3.8
- `#19 <https://gitlab.com/localg-host/watchghost/issues/19>`_ - Add Logger command
- `#21 <https://gitlab.com/localg-host/watchghost/issues/21>`_ - Get a 404 error when open watcher detail in a new tab
- `#23 <https://gitlab.com/localg-host/watchghost/issues/23>`_ - write doc for influxdb logger
- `#24 <https://gitlab.com/localg-host/watchghost/issues/24>`_ - Bug with check_now
- `#26 <https://gitlab.com/localg-host/watchghost/issues/26>`_ - add a "deploy" section in the doc
- `#27 <https://gitlab.com/localg-host/watchghost/issues/27>`_ - Use toml identifier as slug in URL
- `#28 <https://gitlab.com/localg-host/watchghost/issues/28>`_ - improve startup performance
- `#29 <https://gitlab.com/localg-host/watchghost/issues/29>`_ - Use optional imports
- `#39 <https://gitlab.com/localg-host/watchghost/issues/39>`_ - Adds --host and --port CLI parameters
- `#41 <https://gitlab.com/localg-host/watchghost/issues/41>`_ - InfluxDB does not initialize with it config


Contributors:

- Arnault Chazareix
- Arthur Vuillard
- Clement Nieto
- Florent Grenaille
- François Guérin
- Guillaume Ayoub
- Lucie Anglade
- Mathieu Bridon
- Meewan
- Mindiell
- Redmood
- Sam

Backward incompatible changes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Python 3.5 has been dropped. Supported Python versions are 3.6, 3.7 and 3.8.

Version 0.3.0
-------------

Released on 2019-07-31

Changes:

- test with Python 3.5, 3.6 and 3.7
- add a Code Of Conduct
- Tornado has been replaced by AioHttp
- reload of app with SIGHUP signal
- try to load config files only if present
- add a logger that stores events in InfluxDB
- add ``get_info`` option to Http
- add a ``Whois`` service
- use minified Javascript and CSS assets
- add a favicon
- hide ``info`` watchers in dashboard
- sort watchers by tags instead of hosts in dashboard
- in watcher detail page, respect newlines in last result response
- use timezones with datetimes objects
- TOML has replaced YAML for configuration files.


Contributors:

- Arthur Vuillard
- Guillaume Ayoub
- grewn0uille
- Sam
- Sylvain Le Gal
- Lucien Deleu

Backward incompatible changes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As TOML replaced YAML for configuration files, you need to update those configuration files.

We provide a tool called `watchghost-json2toml` that make the changes. The process to upgrade is the following:

- stop watchghost 0.2.2 or earlier
- install 0.3.0
- launch `watchghost-json2toml --config /path/to/your/config/dir`
- launch watchghost as usual

Version 0.2.2
-------------

Released on 2018-11-13

Changes:

- fix status for HTTP service when a error occurs before HTTP
- includes default configuration in package (fix #15)
- improve default configuration by addind a description to the HTTP watcher

Contributors:

- Arthur Vuillard

Version 0.2.1
-------------

Released on 2018-11-07

Changes:

- fix in packaging : license field is not meant to receive the full License but only its name

Contributors:

- Arthur Vuillard

Version 0.2.0
-------------

Released on 2018-11-07

Changes:

- web console is rewritten using Vue.js and Bootstrap 4
- Json API endpoints have been added
- fixes in documentation (it displayed Python instead of JSON as configuration examples)
- fix for TLS with Python 3.7
- packaging updates : use of setup.cfg for distributing and all configurations for development

Contributors:

- Arthur Vuillard
- Guillaume Ayoub
- Sylvain Le Gal

Version 0.1.0
-------------

Released on 2018-05-17

Initial release : Your invisible but loud monitoring pet

WatchGhost is a simple to install and simple to configure monitoring server. It is also a modern Python application making great use of asynchronous architecture.

Contributors:

- Arthur Vuillard
- Guillaume Ayoub
- Sam
- Sylvain Le Gal
- Lucie Anglade
- Pierre Charlet
