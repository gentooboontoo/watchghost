Installation
============

Requirements
------------

Watchghost needs Python>=3.6 in order to work.

From source
-----------

.. code-block:: shell

  pip install watchghost
  watchghost

Once Watchghost is installed and launched, you can access its web interface on
port ``8888``. It also installs some configuration in ``~/.config/watchghost``
if none is found.

For your distribution
---------------------

ArchLinux
~~~~~~~~~

The `latest released package <https://aur.archlinux.org/packages/watchghost/>`_ is available on AUR, as well as a package from the `master branch <https://aur.archlinux.org/packages/watchghost-git/>`_.
