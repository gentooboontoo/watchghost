Builtin Services used by Watchers
=================================

Service
-------
Shared configuration to all builtin services. Every other service heritate from this one.

.. code-block:: toml

  [service]
  description = ""
  repeat = 3600
  before = 23:59:59
  after = 00:00:00
  retry = 2
  retry_interval = 15
  enabled = true
  [service.status]
  info = [{ok = True}]
  critical = [{}]

:description: a string representing the watcher
:repeat: the time period between two checks, in seconds
:after: the hour when the checks must start
:before: the hour when the checks must stop
:retry: the number of checks giving the same result before declaring the state as hard
:retry_interval: the time period (in seconds), between two checks when the state is not hard
:status: a mapping between statuses and filters that trigger these statuses
:enabled: if set to `false`, the watcher is disabled

Ping
----
Default configuration:

.. code-block:: toml

  [ping]
  repeat = 60
  timeout = 3
  ip_version = 4

:timeout: time allowed before throwing a timeout
:ip_version: version of the IP protocol (4 or 6)

HTTP
----
Default configuration:

.. code-block:: toml

  [http]
  repeat = 60
  timeout = 5
  url = ""
  ip_version = 4
  request_hostname = false
  allow_redirects = false
  get_info = false
  [http.status]
  error = [{ok = false}, {code = 400}, {code = 401}, {code = 402}, {code = 403}, {code = 404}, {code = 405}, {code = 406}, {code = 407}, {code = 408}, {code = 409}, {code = 410}, {code = 411}, {code = 412}, {code = 413}, {code = 414}, {code = 415}, {code = 416}, {code = 417}, {code = 418}, {code = 419}, {code = 420}, {code = 421}, {code = 422}, {code = 423}, {code = 424}, {code = 425}, {code = 426}, {code = 427}, {code = 428}, {code = 429}, {code = 430}, {code = 431}]
  warning = [{code = 300}, {code = 301}, {code = 302}, {code = 303}, {code = 304}, {code = 305}, {code = 306}, {code = 307}]
  info = [{code = 200}, {code = 201}, {code = 202}, {code = 203}, {code = 204}, {code = 205}, {code = 206}, {code = 207}, {code = 208}, {code = 209}, {code = 210}, {code = 211}, {code = 212}, {code = 213}, {code = 214}, {code = 215}, {code = 216}, {code = 217}, {code = 218}, {code = 219}, {code = 220}, {code = 221}, {code = 222}, {code = 223}, {code = 224}, {code = 225}]
  critical = [{code = 500}, {code = 501}, {code = 502}, {code = 503}, {code = 504}, {code = 505}, {code = 506}, {code = 507}, {code = 508}, {code = 509}, {code = 510}, {code = 511}]

:timeout: time allowed before throwing a timeout
:url: the URL checked by the watcher
:ip_version: version of the IP protocol (4 or 6)
:request_hostname: if set to `true`, only the url will be used for the checks
:get_info: if set to `true`, display data from response (the route must return a json)
:allow_redirects: if set to `true`, allow redirection (can be needed if get_info is set to `true`)
:status: define the error to report based on the http status in response

FTP
---

You have to install `aioftp <https://pypi.org/project/aioftp/>`_ Python package in order to use this watcher.

.. code-block:: toml

  [ftp]
  repeat = 60
  timeout = 5
  ip_version = 4
  url = ""

:timeout: time allowed before throwing a timeout
:url: the URL checked by the watcher
:ip_version: version of the IP protocol (4 or 6)


SecuredSocket
-------------

Verify a certificate validity

.. code-block:: toml

  [securedsocket]
  repeat = 60
  ip_version = 4
  hostname = ""
  port = 443
  minimum_days_left = 30
  [securedsocket.status]
  error = [{hostname_verified = false}, {in_period = false}, {connected = false}]
  warning = [{enough_days_left = false}]
  info = [{}]


:ip_version: version of the IP protocol (4 or 6)
:hostname: name of the host of the certificate
:port: port emiting the the certificate
:minimum_days_left: number of days of validity left in the certificate before an alert
:status: error, warning or info can be throwed depending on different issues:
:hostname_verified: `true` if the hostname is effectively verified by the certificate.
:in_period: `true` the certificate is in the period of validity
:connected: `true` the certificate can be reached
:enough_days_left: `true` if there are more days of validity left than `minimum_days_left`


SSH
---

You have to install `asyncssh <https://pypi.org/project/asyncssh/>`_ Python package in order to use this watcher.

.. code-block:: toml

  [ssh]
  command = []
  [ssh.status]
  error = [{exit_code = 2}]
  warning = [{exit_code = 1}]
  info = [{exit_code = 0}]

:command: the command and the parameters to be executed on the remote server
:exit_code: code returned by the command


Shell
-----

.. code-block:: toml

  [shell]
  timeout = 10
  command = ""
  [shell.status]
  error = [{return_code = 2}]
  warning = [{return_code = 1}]
  info = [{return_code = 0}]
  critical = [{}]

:timeout: time allowed before throwing a timeout
:command: the command executed on the watchghost server
:return_code: the code returned by the command

whois.Expire
------------

You have to install `python-whois <https://pypi.org/project/python-whois/>`_ Python package in order to use this watcher.

.. code-block:: toml

  [whois-expire]
  service = "whois.Expire"
  domain = "localg.host"

  # Optional Settings
  minimum_days_left_warning = 28
  minimum_days_left_error = 7

External services
-----------------

It is possible to use your own watchers in WatchGhost. You have to install an
external module including a class with a ``config`` dict and a ``check`` async
method:

.. code-block:: python

  class Dummy:
      config = {}

      @staticmethod
      async def check(server, config):
          return {'ok': True, 'message': 'OK'}

If your ``Dummy`` class is included in a module called ``dummy.py``, you can
configure your watcher this way:

.. code-block:: toml

  [external-service]
  description = "Dummy Service"
  service = "dummy.Dummy"
